#!/usr/bin/env bash

export SUDO_ASKPASS=/usr/bin/ksshaskpass
date=$(date +%Y%m%d)
log=./$date.log  

set -o errexit
set -o nounset
set -o pipefail
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
    echo 'Usage: ./install.sh

This is a bash script to Install and configure a streaming computer.

'
    exit
fi

cd "$(dirname "$0")"

main() {
    sudo --askpass add-apt-repository ppa:ansible/ansible -y
    sudo --askpass apt install ansible -y

    sudo --askpass ./run.sh | tee -a $log 2>&1
}

main "$@"
