<img style="float: left;" src="docs/assets/logo.png" alt="logo" width="300"/>

## <div style="text-align: right"></div>
## <div style="text-align: right"><span style="color:#5dbaebff">Runbook: </span>Example Streaming Deployment Playbook</div>


</br>
</br>

Ansible Playbook for Streaming Deployments

## <span style="color:#5dbaebff">Table of content</span>

- [Initial Setup](#initial-setup)
- [Assumptions](#assumptions)
- [Example](#example)
- [Default Variables](#default-variables)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## <span style="color:#5dbaebff">Asumptions</span>
* Kubuntu 22.04 Desktop
* Minimum 4 Core and 8 GB RAM
* Executed locally on the computer that is being setup as a streaming computer


## <span style="color:#5dbaebff">Setup</span>

1. Install Git and Ansible

### <span style="color:#5dbaebff">Ubuntu</span>

```Bash
sudo apt install git ansible
```

If you need a newer version of ansible you will need to add the ansible ppa
```bash
sudo add-apt-repository ppa:ansible/ansible
```
```bash
sudo apt-get update
```

2. Clone Repository


```bash
git clone git@gitlab.com:altispeed/public/ansible/templates/playbooks/streaming-deploy.git
```
```bash
cd /path/to/playbook/repo
```


3. Setup Inventory folder and Vars and Hosts files


### <span style="color:#5dbaebff">Examples</span>


#### <span style="color:#5dbaebff">Host File</span>


```ini
stream1 ansible_connection=local ansible_user=root


[base]
stream1


[workstation]
stream1
```


#### <span style="color:#5dbaebff">Vars File</span>


**Path:** /path/to/playbook/repo/inventory/host_vars/example/vars.yml


```yaml
# What group of ssh keys to deploy (i.e. prod or dev)
base_server_environment: ""


# Admin account username
base_admin: "localadmin"
```


#### <span style="color:#5dbaebff">Vault File</span>


**Path:** /path/to/playbook/repo/inventory/host_vars/example/vault.yml


```yaml
vault_base_admin_password: changem3


vault_workstation_user_password: "go live now"
```


4. Make sure that run.sh and install.sh are executable


5. Run install.sh by clicking on it and entering your password


```bash
sudo ./install.sh
```



## <span style="color:#5dbaebff">Default Variables</span>

### <span style="color:#5dbaebff">base_additional_ports</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_additional_ports:
  - port: 22
    proto: tcp
    application: ssh
```

### <span style="color:#5dbaebff">base_admin</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_admin: localadmin
```

### <span style="color:#5dbaebff">base_admin_password</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_admin_password: changem3
```

### <span style="color:#5dbaebff">base_key_group</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_key_group: '{{ base_server_environment }}'
```

### <span style="color:#5dbaebff">base_keys_source</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_keys_source: file
```

### <span style="color:#5dbaebff">base_reboot</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_reboot: true
```

### <span style="color:#5dbaebff">base_server_environment</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_server_environment: ''
```

### <span style="color:#5dbaebff">base_snapd_enabled</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_snapd_enabled: false
```

### <span style="color:#5dbaebff">base_timezone</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_timezone: Etc/UTC
```

### <span style="color:#5dbaebff">workstation_companion_branch</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_companion_branch: stable
```

### <span style="color:#5dbaebff">workstation_companion_database_file</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_companion_database_file: ''
```

### <span style="color:#5dbaebff">workstation_companion_database_root</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_companion_database_root: ''
```

### <span style="color:#5dbaebff">workstation_companion_minor_version</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_companion_minor_version: 2
```

### <span style="color:#5dbaebff">workstation_companion_version</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_companion_version: 3.2
```

### <span style="color:#5dbaebff">workstation_default_autostarts</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_default_autostarts:
  - name: Firefox
    filename: firefox
    exec: snap run firefox
```

### <span style="color:#5dbaebff">workstation_nvidia_version</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_nvidia_version: 550
```

### <span style="color:#5dbaebff">workstation_packages</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_packages:
  - base
  - vpn
  - laptop
  - media
  - virtualization
  - development
  - messaging
  - medical
```

### <span style="color:#5dbaebff">workstation_streaming_obs_assets</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_streaming_obs_assets:
  src: streaming-profiles/default/assets/Assets
  dest: /home/{{ workstation_user }}/broadcast/Assets
```

### <span style="color:#5dbaebff">workstation_streaming_obs_config</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_streaming_obs_config: streaming-profiles/default/configs/obs/
```

### <span style="color:#5dbaebff">workstation_streaming_obs_record_location</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_streaming_obs_record_location: broadcast/raw-recordings
```

### <span style="color:#5dbaebff">workstation_theme</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_theme:
  name: altispeed
  applet_blocks: '{{ workstation_plasma_default }}'
```

### <span style="color:#5dbaebff">workstation_usbguard</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_usbguard:
  new: false
  add: false
```

### <span style="color:#5dbaebff">workstation_user</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_user: localadmin
```

### <span style="color:#5dbaebff">workstation_user_password</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_user_password: changem3
```

### <span style="color:#5dbaebff">workstation_yubikey</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
workstation_yubikey:
  pin:
    old: '123456'
    new: '123456'
  generate: false
```

## <span style="color:#5dbaebff">Discovered Tags</span>

**_always_**

**_install-updates_**

**_lockdown_**

**_setup-admin_**

**_setup-all_**

**_setup-ssh_**

**_setup-workstation_**

**_update-base_**


## <span style="color:#5dbaebff">Dependencies</span>

None.

## <span style="color:#5dbaebff">License</span>

GPL-3.0-only

## <span style="color:#5dbaebff">Author</span>

Dark Decoy
