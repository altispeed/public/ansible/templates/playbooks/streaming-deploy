#!/usr/bin/env bash

set -o errexit

ansible-galaxy install --role-file requirements.yml --roles-path ./roles --force

ansible-playbook --inventory inventory/hosts setup.yml --tags "setup-ssh,update-base"

ansible-playbook --inventory inventory/hosts setup.yml --tags "setup-all"

reboot
